var gulp = require('gulp');
var concat = require('gulp-concat');
var es = require('event-stream');
var htmlreplace = require('gulp-html-replace');
var replace = require('gulp-replace');
var rename = require("gulp-rename");
var clean = require('gulp-clean');
var minify = require('gulp-minify');
var sftp = require('gulp-sftp');

gulp.task('clean', function() {
    /* Cleanup */
    return gulp.src('release/', {read: false})
        .pipe(clean());
})

gulp.task('default', ['clean'], function () {
    return es.concat(
        /* Merge all javascript files and replace template path */
        gulp.src(['app/components/**/*.js', 'app/app.js']
        ).pipe(replace(/components\/\w+\/(\w+)/g,'components/$1'))
            .pipe(concat('csc.js'))
            .pipe(gulp.dest('release/js/')),

        /* Merge all bundle components javascript */
        gulp.src(['app/bower_components/jquery/dist/jquery.js',
            'app/bower_components/angular/angular.js',
            'app/bower_components/angular-i18n/angular-locale_de-at.js',
            'app/bower_components/angular-route/angular-route.js',
            'app/bower_components/angular-resource/angular-resource.js',
            'app/bower_components/angular-cookies/angular-cookies.js',
            'app/bower_components/angular-ui-validate/dist/validate.js',
            'app/bower_components/bootstrap/dist/js/bootstrap.js',
            'app/bower_components/angular-base64-upload/dist/angular-base64-upload.js',
            'app/bower_components/ngstorage/ngStorage.js',
            'app/bower_components/angular-utils-pagination/dirPagination.js',
            'app/bower_components/angular-sanitize/angular-sanitize.js',
            'app/bower_components/showdown/compressed/Showdown.js',
            'app/bower_components/angular-markdown-directive/markdown.js'
        ]).pipe(concat('components.js'))
            .pipe(minify({noSource: true}))
            .pipe(gulp.dest('release/js/')),

        /* Summarize all templates in one directory */
        gulp.src('app/components/**/*.html')
            .pipe(replace(/components\/\w+\/(\w+)/g,'components/$1'))
            .pipe(rename({dirname: ''}))
            .pipe(gulp.dest('release/components')),

        /* Copy all logos */
        gulp.src('app/style/*.*')
            .pipe(gulp.dest('release/style/')),

        /* Copy custom style file */
        gulp.src('app/app.css')
            .pipe(rename('csc.css'))
            .pipe(gulp.dest('release/style/')),

        /* Merge all other css files */
        gulp.src(['app/bower_components/bootstrap/dist/css/bootstrap.css',
            'app/bower_components/components-font-awesome/css/font-awesome.css'])
            .pipe(concat('components.css'))
            .pipe(gulp.dest('release/style/')),

        /* Copy all fonts */
        gulp.src('app/bower_components/components-font-awesome/fonts/*.*')
            .pipe(gulp.dest('release/fonts/')),

        /* Copy and modify main html file */
        gulp.src('app/index.html')
            .pipe(replace(/components\/\w+\/(\w+)/g,'components/$1'))
            .pipe(htmlreplace({
                js: {
                    src: ['components-min.js', 'csc.js'],
                    tpl: '<script src="js/%s"></script>'
                },
                css: {
                    src: ['components.css', 'csc.css'],
                    tpl: '<link rel="stylesheet" href="style/%s">'
                }
            }))
            .pipe(gulp.dest('release/'))
    );
});

gulp.task('upload', ['default'], function() {
    gulp.src('release/**/*')
        .pipe(sftp({
            host: 'its.fh-campuswien.ac.at',
            port: 8022,
            auth: 'itsCredentials',
            remotePath: '/var/www/hackathon/frontend/'
        }));
});
