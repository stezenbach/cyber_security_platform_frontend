angular.
module('cscFrontend.model_challengeset', ['ngResource']).
factory('ChallengeSet', ['config', '$resource',
    function(config, $resource) {
        return $resource(config.apiUrl + '/challengesets/:challengeSetId', {}, {

        });
    }
]);