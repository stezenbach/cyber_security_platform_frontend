angular.
module('cscFrontend.model_challengeDescription', ['ngResource']).
factory('ChallengeDescription', function(config, $resource) {
        return $resource(config.apiUrl + '/challenges/:challengeId/description', {}, {

        });
    }
);